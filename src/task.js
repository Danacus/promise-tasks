// @flow

export default class Task extends Promise<any> {
  data: Object
  percent: number
  progListeners: Array<(any) => mixed>
  state: string

  constructor(executor: (any, any, any) => mixed, data: Object = {}) {
    let self: Task

    super((resolve, reject) => executor(
      value => { self.doResolve(); return resolve(value) }, err => { self.doReject(); return reject(err) }, value => { self.setProgress(value) }
    ))

    self = this

    this.data = data
    this.percent = 0
    this.state = 'PENDING'
  }

  progress(cb: (any) => mixed): Task {
    if (!this.progListeners) {
      this.progListeners = []
    }
    this.progListeners.push(cb)
    return this
  }

  then(value: any): Task {
    super.then(value)
    return this
  }

  catch(value: any): Task {
    super.catch(value)
    return this
  }

  doResolve() {
    this.progListeners = []
    this.state = 'DONE'
  }

  doReject() {
    this.progListeners = []
    this.state = 'ERROR'
  }

  setProgress(prog: number) {
    this.percent = prog
    this.progListeners.forEach(listener => listener(prog))
  }
}
