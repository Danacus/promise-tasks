// @flow

import Task from './task'

function test() {
  return new Task((resolve, reject, progress) => {
    let prog = 0
    let interval = setInterval(() => {
      prog += 10
      progress(prog)
    }, 500)

    setTimeout(() => {
      resolve()

      clearInterval(interval)
    }, 3000)
  }, {
    name: 'Test Task'
  })
}

let task = test().then(() => console.log('Done!')).catch(console.error)
console.log(task)
task.progress(console.log)

task.then(() => testAsync())

async function testAsync() {
  console.log(task)
  let task2 = test()
  task2.progress(console.log)
  await task2
  console.log('Done2!')
}
