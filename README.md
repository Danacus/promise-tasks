# Promise-tasks

Track progress of you Promises with "Tasks". 
Tasks extend the builtin Promise class to allow you to keep track of progress in your asynchrounous operations.
They also allow you to assign additional information to a Task, to help you keep track of your ongoing tasks at a centralised location in your code, if you would like to do so.

Since they inherit a Promise, they are a drop-in replacement for Promises.
This also makes them compatible with the new async/await keywords.


## Installation

`yarn add promise-tasks` or
`npm install --save promise-tasks`

## Usage

```javascript

import Task from 'promise-tasks'

// A test function that returns a Task
function test() { 
  // Simply add progress to the arguments
  return new Task((resolve, reject, progress) => {
    let prog = 0
    let interval = setInterval(() => {
      prog += 10
      // Call progress with a numeric value to update progress
      progress(prog)
    }, 500)

    setTimeout(() => {
      // Resolve the same way you would with a Promise
      resolve()

      // Not really requried, since all progress listeners will be removed after resolving or rejecting
      clearInterval(interval)
    }, 3000)
  }, 
  // Here you can add additional data
  // You can retrieve this data in the data property of your task object
  {
    name: 'Test Task'
  })
}

// Here's an example of how you can track it's progress
let task = test().progress(console.log).then(() => console.log('Done!')).catch(console.error)
/*
Output:
10
20
30
40
50
Done!
*/

console.log(task)
/*
Output:
Task { <pending>, data: { name: 'Test Task' }, percent: 0, state: 'PENDING' }
*/
// Notice that there is '<pending>' in this class, this is inherited from Promise
// Besides that, there also is a state property
// Possibel values are 'PENDING', 'DONE', 'ERROR'

// You can have as many progress listeners as you want, anywhere you want.
task.progress(console.log)

//Let's also try them with async/await, after our first task resolves
task.then(() => testAsync())

async function testAsync() {
  let task2 = test()
  task2.progress(console.log)
  await task2
  console.log('Done!')
}

// This async function will log the same output as with our previous task

// Here's an idea of what you could use this for

// Let's create some tasks
let tasks = [
  new Task((resolve, reject, progress) => /*Some async code*/, {name: 'Downloading memes...'}),
  new Task((resolve, reject, progress) => /*Some async code*/, {name: 'Downloading cute cat pics...'})  
]

// You can always add a new task
tasks.push(new Task(/*...*/))

// Let's see how our tasks are doing
tasks.forEach(task => {
  // Let's filter out the tasks that are already done
  if (task.state !== 'DONE') {
    // Remeber you need to use task.data.name and not task.name
    // task.percent is the current progress of a task (I could name it progress, since that's already a method)
    console.log(`${task.data.name}: ${task.percent}%`)
  }
})

/*
This may output something like this:

Downloading memes...: 24%
Downloading cute cat pics...: 68%
*/

// This may be useful for a GUI application where the user can start multiple downloads at different times,
// but you still want to show a summary of all ongoing tasks.

```